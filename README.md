# Mekamon Smart Module - mekamon_msgs

ROS package providing custom ROS messages and services used by mekamon\_base and mekamon\_remote.
